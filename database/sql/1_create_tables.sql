create schema api;

-- In production app wouldn't have the password here.
-- i.e. make the user with psql shell script and get password from env.
create user simple_issue_tracker_api with password 'sitapassword';
grant usage on schema api to simple_issue_tracker_api;

create or replace function trigger_set_timestamp()
returns trigger as $$
begin
  new.updated_at = now();
  return new;
end;
$$ language plpgsql;

create type issue_state as enum ('open', 'pending', 'closed');

create table if not exists api.issue (
  id serial primary key,
  title text not null,
  description text not null,
  state issue_state,
  created_at timestamp not null default now(),
  updated_at timestamp
);

grant select, update on api.issue to simple_issue_tracker_api;

create trigger set_timestamp
before update on api.issue
for each row
execute procedure trigger_set_timestamp();