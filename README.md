# simple-issue-tracker

Simple issue tracker – code test for the Schibsted Sports team
## Description

My implementation consists of three main components: React SPA, Node API and a Postgres database.
## How to Run

Requirements:
  - Docker
  - npm

Clone and navigate to repository root

Start backend with
```
docker-compose up --build
```

Start frontend with:
```
(cd frontend/simple-issue-tracker && npm install && npm run start)
```

Open http://localhost:3000 to view the React app

## Technology selection and libraries

### Frontend

For the frontend app I chose to go with a React SPA for the following reasons:
  - Is in the Javascript environment (requirement)
  - Suitability to create modern web applications
  - Personal experience, so development was fast
  - One of the tools mentioned in the job advertisement

#### Libraries

- I decided to use a design system (Zendesk Garden in this case) with ready React components to create a nice(ish) looking and accessible app without spending too much time on UI design.
- Used styled components for CSS-in-JS approach.
- Prepared Cypress for testing. Works well with e2e testing in a small application like this. Can cover much of the frontend and backend code with just a few tests. Tests still to be implemented in future versions.
- Axios for making API requests
- React for state management. No need for a library in an app this small.

### Backend

For the backend I chose to build a Node.js server with the Hapi framework and a Postgres database for the following reasons:
  - Is in the Javascript environment (requirement)
  - Have only used Express.js before with Node.js and the job advertisement mentioned Hapi, so decided to give it a shot and learn something new. After a little research I came to the conclusion that Hapi seemed a very suitable tool for building an API for this app.
  - Postgres database for data storage. Could have used many other tools to store the data in the app since the data model is very simple. Decided to go with Postgres because ready tooling with Node.js and personal experience so development was quick.
  - If this was not a coding test I would have used a GraphQL server such as Hasura for a CRUD app like this, but that would have abstracted away much of the backend code so I didn't find it suitable in this case where coding skills need to be assessed.
  - I am running Node and Postgres in Docker containers and using docker-compose to define and run the services.

## Possible improvements

- Test coverage
- Loading and empty components for better UX
- Constraint to db or api level for updating issue state
- Additional features such as add, delete and modify issue, filtering, search and pagination improvements
- Maybe a different layout such as table style with open, pending and closed issues separated in their own tables would be better for this kind of app.
- CI/CD pipeline and production deployment
- Improved error handling in frontend and API
- Now the frontend app is hitting the API directly, could go i.e. through a proxy for security
- Authentication?
- Security: Now the .env file and passwords for db are in git for convenience and sake of this project. Would never store them there in a production project.
- Now the system is using a monorepo, as it grows could be split into multiple git repos/submodules for each service
- Offset based pagination becomes inefficient at some point when the amount of data grows.
