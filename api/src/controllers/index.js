'use strict'

// Get all issues
const getIssuesQuery = (db, limit, offset) => db`
    select
      id,
      title,
      description,
      state,
      created_at,
      updated_at,
      count(*) over() as row_count
    from api.issue
    order by created_at desc
    limit ${limit}
    offset ${offset};
  `

// Update issue
const updateIssuesQuery = (db, id, state) => db`
    update api.issue
    set state = ${state}
    where id = ${id}
    returning *;
  `

module.exports = {
  getIssuesQuery,
  updateIssuesQuery
}
