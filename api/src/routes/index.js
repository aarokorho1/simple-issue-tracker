'use strict'

const Joi = require('joi')
const { getIssuesHandler, updateIssuesHandler } = require('../services/index')

const getIssues = {
  method: 'GET',
  path: '/api/issues',
  options: {
    validate: {
      query: Joi.object({
        limit: Joi.number().integer().min(1).max(100).default(5),
        offset: Joi.number().integer().default(0)
      }).options({ stripUnknown: true })
    }
  },
  handler: getIssuesHandler
}

const updateIssue = {
  method: 'PATCH',
  path: '/api/issues/{id}',
  options: {
    validate: {
      payload: Joi.object({
        state: Joi.string().valid('closed', 'pending')
      }).options({ stripUnknown: true }),
      params: Joi.object({
        id: Joi.number().integer().min(1)
      }).options({ stripUnknown: true })
    }
  },
  handler: updateIssuesHandler
}

module.exports = [getIssues, updateIssue]
