'use strict'

const Boom = require('@hapi/boom')

const { getIssuesQuery, updateIssuesQuery } = require('../controllers/index')

const getIssuesHandler = async (request, h) => {
  try {
    const db = h.sql
    const { limit, offset } = request.query
    const issues = await getIssuesQuery(db, limit, offset)
    return h.response({ data: issues }).code(200)
  } catch (err) {
    return Boom.badImplementation()
  }
}

const updateIssuesHandler = async (request, h) => {
  try {
    const db = h.sql
    const { id } = request.params
    const { state } = request.payload
    const issue = await updateIssuesQuery(db, id, state)
    return h.response({ data: issue }).code(200)
  } catch (err) {
    return Boom.badImplementation()
  }
}

module.exports = {
  getIssuesHandler,
  updateIssuesHandler
}
