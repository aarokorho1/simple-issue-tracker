'use strict'

const Hapi = require('@hapi/hapi')
const dotenv = require('dotenv')
const routes = require('./routes')
const sqlPlugin = require('./plugins/sql')

const init = async () => {
  dotenv.config()

  const server = Hapi.server({
    port: process.env.PORT || 3000,
    host: process.env.HOST || '0.0.0.0',
    routes: {
      cors: true
    }
  })

  await server.register(sqlPlugin)

  server.route(routes)

  await server.start()
  console.log('Server running on %s', server.info.uri)
}

process.on('unhandledRejection', (err) => {
  console.log(err)
  process.exit(1)
})

init()
