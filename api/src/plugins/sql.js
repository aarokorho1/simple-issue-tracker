'use strict'

const postgres = require('postgres')

module.exports = {
  name: 'sql',
  version: '1.0.0',
  register: async (server) => {
    const sql = postgres(process.env.DATABASE_URL)
    server.events.once('stop', () => sql.end({ timeout: 0 }))
    server.decorate('toolkit', 'sql', sql)
  }
}
