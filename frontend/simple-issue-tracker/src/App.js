import { Header } from './components/header/Header.jsx'
import { Container } from './components/layout/container/Container.jsx'
import { PaginatedList } from './components/paginatedList/PaginatedList.jsx'
import { ThemeProvider } from '@zendeskgarden/react-theming'

function App() {
  return (
    <ThemeProvider>
      <Container>
        <Header />
        <PaginatedList />
      </Container>
    </ThemeProvider>
  )
}

export default App
