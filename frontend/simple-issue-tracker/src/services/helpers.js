import { getColor } from '@zendeskgarden/react-theming'

export const issueColor = (issueState, hue) => {
  const hueNumber = hue === 'light' ? 100 : hue === 'medium' ? 400 : 600
  switch (issueState) {
    case 'closed':
      return getColor('green', hueNumber)
    case 'pending':
      return getColor('yellow', hueNumber)
    default:
      return getColor('crimson', hueNumber)
  }
}

export const snakeToCamel = (arr) => {
  return arr.map((obj) =>
    Object.entries(obj).reduce((acc, [key, val]) => {
      const modifiedKey = key.replace(/_([a-z])/g, (g) => g[1].toUpperCase())
      return {
        ...acc,
        ...{ [modifiedKey]: val }
      }
    }, {})
  )
}
