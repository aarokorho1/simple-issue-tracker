import axios from 'axios'
import { snakeToCamel } from './helpers'

const ISSUES_API = axios.create({
  baseURL: 'http://localhost:5000/api/'
})

const getIssues = async (limit, offset) => {
  try {
    const response = await ISSUES_API.get(
      `issues?limit=${limit}&offset=${offset}`
    )
    if (response.status === 200) {
      const { data } = response.data
      return {
        issueTotal: data.length > 0 ? data[0].row_count : 0,
        data: snakeToCamel(data)
      }
    }
  } catch (err) {
    throw new Error(err)
  }
}

const updateIssue = async (issueId, newState) => {
  try {
    const response = await ISSUES_API.patch(`issues/${issueId}`, {
      state: newState
    })
    if (response.status === 200) {
      const { data } = response.data
      return snakeToCamel(data)[0]
    }
  } catch (err) {
    throw new Error(err)
  }
}

export const API = {
  getIssues,
  updateIssue
}
