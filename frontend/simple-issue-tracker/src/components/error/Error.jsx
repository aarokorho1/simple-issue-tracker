import { Alert, Title, Close } from '@zendeskgarden/react-notifications'

export const Error = (props) => {
  const { handleOnClick, message } = props
  return (
    <Alert type="error">
      <Title>Error</Title>
      {message}
      <Close aria-label="Close Alert" onClick={handleOnClick} />
    </Alert>
  )
}
