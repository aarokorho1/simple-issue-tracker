import { MD, XXL } from '@zendeskgarden/react-typography'
import { StyledHeader } from './StyledHeader'

export const Header = () => {
  return (
    <StyledHeader>
      <XXL>SimplIT</XXL>
      <MD>Simple issue tracker</MD>
    </StyledHeader>
  )
}
