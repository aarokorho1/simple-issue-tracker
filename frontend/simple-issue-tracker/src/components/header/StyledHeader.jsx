import styled from 'styled-components'
import { getColor } from '@zendeskgarden/react-theming'

export const StyledHeader = styled.div`
  border-radius: ${(props) => props.theme.borderRadii.md};
  border: ${(props) => props.theme.borders.md + getColor('grey', 600)};
  background-color: ${getColor('grey', 100)};
  padding: ${(props) => props.theme.space.lg} ${(props) => props.theme.space.md};
`
