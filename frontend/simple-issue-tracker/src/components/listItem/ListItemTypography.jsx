import { MD, LG } from '@zendeskgarden/react-typography'
import styled from 'styled-components'
import { issueColor } from '../../services/helpers'

export const IssueState = styled(LG)`
  color: ${(props) => issueColor(props.issueState)};
  padding-right: 0.5em;
  :first-letter {
    text-transform: capitalize;
  }
`

export const CapitalLG = styled(LG)`
  :first-letter {
    text-transform: capitalize;
  }
`

export const CapitalMD = styled(MD)`
  :first-letter {
    text-transform: capitalize;
  }
`
