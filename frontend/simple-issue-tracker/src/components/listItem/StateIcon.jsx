import {
  FaExclamationCircle,
  FaCheckCircle,
  FaQuestionCircle
} from 'react-icons/fa'
import { issueColor } from '../../services/helpers'

export const StateIcon = ({ issueState }) => {
  const iconSize = '1.5em'
  switch (issueState) {
    case 'open':
      return (
        <FaExclamationCircle color={issueColor(issueState)} size={iconSize} />
      )
    case 'pending':
      return <FaQuestionCircle color={issueColor(issueState)} size={iconSize} />
    default:
      return <FaCheckCircle color={issueColor(issueState)} size={iconSize} />
  }
}
