import { SM } from '@zendeskgarden/react-typography'
import { IssueState, CapitalMD, CapitalLG } from './ListItemTypography'
import { StyledListItem, FlexCol } from './StyledListItem'
import { Row, Col } from '@zendeskgarden/react-grid'
import { CloseIssueButton, SetIssuePendingButton } from '../button/IssueButton'
import { StateIcon } from './StateIcon'

export const ListItem = (props) => {
  const {
    title,
    description,
    issueState,
    createdAt,
    updatedAt,
    id,
    handleOnClickItem
  } = props

  return (
    <StyledListItem issueState={issueState}>
      <Row>
        <Col>
          <CapitalLG>{`${title} #${id}`}</CapitalLG>
        </Col>
        <FlexCol>
          <IssueState issueState={issueState}>{issueState}</IssueState>
          <StateIcon issueState={issueState} />
        </FlexCol>
      </Row>
      <Row>
        <Col size={11}>
          <CapitalMD>{description}</CapitalMD>
        </Col>
      </Row>
      <Row>
        <Col>
          <SM isBold>{`Created at: ${new Date(createdAt).toLocaleDateString(
            'no-NO'
          )}`}</SM>
          {updatedAt && (
            <SM isBold>{`${
              issueState === 'closed' ? 'Issue closed ' : 'Set pending at '
            }: ${new Date(updatedAt).toLocaleDateString('no-NO')}`}</SM>
          )}
        </Col>
        <Col textAlign={'end'}>
          {issueState === 'open' ? (
            <>
              <SetIssuePendingButton
                onClick={() => handleOnClickItem(id, 'pending')}
              />
              <CloseIssueButton
                onClick={() => handleOnClickItem(id, 'closed')}
              />
            </>
          ) : issueState === 'pending' ? (
            <CloseIssueButton onClick={() => handleOnClickItem(id, 'closed')} />
          ) : undefined}
        </Col>
      </Row>
    </StyledListItem>
  )
}
