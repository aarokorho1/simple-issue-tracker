import styled from 'styled-components'
import { Grid, Col } from '@zendeskgarden/react-grid'
import { issueColor } from '../../services/helpers'

export const StyledListItem = styled(Grid)`
  border: ${(props) =>
    props.theme.borders.md + issueColor(props.issueState, 'medium')};
  border-radius: ${(props) => props.theme.borderRadii.md};
  padding: ${(props) => props.theme.space.md};
  margin: ${(props) => props.theme.space.md} auto;
  > *:not(:first-child) {
    margin-top: ${(props) => props.theme.space.md};
  }
`

export const FlexCol = styled(Col)`
  display: flex;
  justify-content: flex-end;
`
