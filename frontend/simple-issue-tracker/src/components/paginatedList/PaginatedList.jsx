import React, { useState, useEffect } from 'react'
import { API } from '../../services/API'
import { Pagination } from '../pagination/Pagination'
import { ListItem } from '../listItem/ListItem'
import { Error } from '../error/Error'

const DEFAULT_PAGE_SIZE = 5
const INITIAL_PAGE = 1

export const PaginatedList = () => {
  const [issues, setIssues] = useState(null)
  const [currentPage, setCurrentPage] = useState(INITIAL_PAGE)
  const [issueTotal, setIssueTotal] = useState(null)
  const [fetchError, setFetchError] = useState(null)
  const [updateError, setUpdateError] = useState(null)

  useEffect(() => {
    ;(async () => {
      try {
        const offset = (currentPage - 1) * DEFAULT_PAGE_SIZE
        const response = await API.getIssues(DEFAULT_PAGE_SIZE, offset)
        setIssues(response.data)
        setIssueTotal(response.issueTotal)
      } catch (err) {
        setFetchError({ message: err.message })
      }
    })()
  }, [currentPage])

  const setPage = (newPage) => {
    setCurrentPage(newPage)
  }

  const handleOnClickItem = async (id, newIssueState) => {
    try {
      const response = await API.updateIssue(id, newIssueState)
      const updatedList = issues.map((issue) =>
        issue.id === id ? response : issue
      )
      setIssues(updatedList)
    } catch (err) {
      setUpdateError({ id, message: err.message })
    }
  }

  return (
    <div>
      {fetchError ? (
        <Error
          message={fetchError.message}
          handleOnClick={() => setFetchError(null)}
        />
      ) : undefined}
      {issues &&
        issues.map(
          ({ id, title, description, state, createdAt, updatedAt }) => (
            <React.Fragment key={id}>
              <ListItem
                id={id}
                title={title}
                description={description}
                issueState={state}
                createdAt={createdAt}
                updatedAt={updatedAt}
                handleOnClickItem={handleOnClickItem}
              />
              {updateError && updateError.id === id ? (
                <Error
                  message={updateError.message}
                  handleOnClick={() => setUpdateError(null)}
                />
              ) : undefined}
            </React.Fragment>
          )
        )}
      <Pagination
        totalPages={Math.ceil(issueTotal / DEFAULT_PAGE_SIZE)}
        currentPage={currentPage}
        setPage={setPage}
      />
    </div>
  )
}
