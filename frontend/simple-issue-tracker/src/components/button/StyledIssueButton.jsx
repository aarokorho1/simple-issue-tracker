import styled from 'styled-components'
import { Button } from '@zendeskgarden/react-buttons'
import { issueColor } from '../../services/helpers'

export const StyledIssueButton = styled(Button)`
  margin-left: ${(props) => props.theme.space.sm};
  color: ${(props) => issueColor(props.type)};
  border-color: ${(props) => issueColor(props.type)};
  &:hover {
    background-color: ${(props) => issueColor(props.type, 'light')};
    color: ${(props) => issueColor(props.type)};
    border-color: ${(props) => issueColor(props.type)};
  }
  &:active {
    opacity: 0.7;
  }
`
