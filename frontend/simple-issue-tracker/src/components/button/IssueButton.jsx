import { StyledIssueButton } from './StyledIssueButton'

export const CloseIssueButton = (props) => (
  <StyledIssueButton size={'small'} {...props} type={'closed'}>
    {'Close issue'}
  </StyledIssueButton>
)
export const SetIssuePendingButton = (props) => (
  <StyledIssueButton size={'small'} {...props} type={'pending'}>
    {'Set pending'}
  </StyledIssueButton>
)
