import { StyledPagination } from './StyledPagination'
import { Pagination as ZendeskPagination } from '@zendeskgarden/react-pagination'

export const Pagination = (props) => {
  const { totalPages, currentPage, setPage } = props
  return (
    <StyledPagination>
      <ZendeskPagination
        totalPages={totalPages}
        currentPage={currentPage}
        onChange={setPage}
      />
    </StyledPagination>
  )
}
