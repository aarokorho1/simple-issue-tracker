import styled from 'styled-components'

export const StyledPagination = styled.div`
  padding: ${(props) => props.theme.space.md};
`
