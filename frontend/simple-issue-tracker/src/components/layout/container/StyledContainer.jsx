import styled from 'styled-components'

export const StyledContainer = styled.div`
  max-width: 720px;
  margin: ${(props) => props.theme.space.md} auto;
`
