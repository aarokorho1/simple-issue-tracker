import { StyledContainer } from './StyledContainer'

export const Container = (props) => {
  return <StyledContainer>{props.children}</StyledContainer>
}
