import styled from 'styled-components'
import { Row } from '@zendeskgarden/react-grid'

export const PaddedRow = styled(Row)`
  padding-top: ${(props) => props.theme.space.xxs};
  padding-bottom: ${(props) => props.theme.space.xxs};
`
